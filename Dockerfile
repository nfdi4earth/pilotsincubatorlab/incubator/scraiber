FROM python:3.9.14-buster

RUN pip install --upgrade pip
RUN pip install Django psycopg2-binary numpy hyperspy imageio ruamel.yaml torch torchvision pandas seaborn django_tables2 celery-progress celery

COPY . /src
WORKDIR /src/scraiber

RUN pip install gunicorn
RUN python manage.py migrate
CMD ["gunicorn", "--config", "gunicorn_config.py", "scraiber.wsgi"]

#
# python manage.py runserver 0.0.0.0:8888
