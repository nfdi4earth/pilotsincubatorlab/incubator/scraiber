# NFDI4Earth Incubator Project Scr🤖iber
This project is part of the [NFDI4Earth](https://www.nfdi4earth.de/).

## Project goals
Scraiber is a web tool for automatic segmentation of olivine crystals in back scatter electron images using deep learning. This project was funded by NFDI4earth and aims to simplify the use of deep learning technology while collecting data to improve segmentation performance.


The service is freely available at http://scraiber.icaml.ikg.uni-hannover.de/

## How to install
This software is available as a docker image. To run it locally run following command:

```cmd
    docker run --rm --shm-size="25G" -p 8888:8888 registry.git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/scraiber:1.0.1

```
For interactive jupyter environment run following command:

```cmd
    
    docker run -v $PWD/scraiber:/src/scraiber -p 8888:8888 registry.git.rwth-aachen.de/nfdi4earth/pilotsincubatorlab/incubator/scraiber:jupyter-1.0.1 jupyter lab --allow-root --ip 0.0.0.0

```

## How to use

Scraiber is a online tool which can be found here: scraiber.icaml.org. 
[Video](https://www.youtube.com/watch?v=SVA4Aw8eqxY)

Scraiber cann also be hosted localy. To do so follow the instructions from the how to install section.

The scripts used for the scraiber can be acces programticaly. Example how to do this you can find the folde `scraiber/examples`.
[Video](https://www.youtube.com/watch?v=HnOQ3MESawk)


## How to cite
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.7744225.svg)](https://doi.org/10.5281/zenodo.7744225)

and, optionally, also to the head of your repository (Settings->General-> expand Badges).

## References
Leichter, A., Almeev, R. R., Wittich, D., Beckmann, P., Rottensteiner, F., Holtz, F., & Sester, M. (2022). Automated segmentation of olivine phenocrysts in a volcanic rock thin section using a fully convolutional neural network. Frontiers in Earth Science 10 (2022), 10, 740638.
[Olivine Map](https://icaml.ikg.uni-hannover.de/olmap/)

## Founding
This work has been funded by the German Research Foundation (NFDI4Earth,
DFG project no. 460036893, https://www.nfdi4earth.de/).


