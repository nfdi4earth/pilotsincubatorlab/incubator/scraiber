from django.apps import AppConfig


class Bcf2TiffConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'bcf2tiff'
