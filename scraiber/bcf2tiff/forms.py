from django import forms
from .models import Input, Converison






class InputForm(forms.ModelForm):
    #file = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))
    class Meta:
        model = Input
        fields = ['vulcano_or_rock_name', 'thinsection_label', 'mineral']

class ConvForm(forms.ModelForm):
    class Meta:
        model = Converison
        fields = ['notes']