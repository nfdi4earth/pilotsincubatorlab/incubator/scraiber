from django.db import models
from django.core.validators import FileExtensionValidator

import django_tables2 as tables


def valid_bcf_file(value):
    print(value)

class Input(models.Model):
    vulcano_or_rock_name = models.CharField(max_length=100)
    thinsection_label = models.CharField(max_length=100)
    mineral = models.CharField(max_length=200)
    #file = models.FileField()


"""
Concerning your question for sample labels. They can be:

1. Sample, or thin section label. Above this level there can be other labels, like project, volcano, rock name, eruption age, Unit etc. But label of thin section is always unique  and it is good to start from this label, to characterize objects which follow below. Others can be added later, I guess...
2. Mineral
3. Mineral size OR population OR generation (phenocryst, subphenocryst or microlite)
4. Zonning, yes OR no, If YES, which type (normal, reversed, complex)
5. shape (idiomorphic - [nicely rounded, oval or faceted] OR disintegrated OR resorbed)
"""

class Converison(models.Model):
    notes = models.CharField(max_length=200)


