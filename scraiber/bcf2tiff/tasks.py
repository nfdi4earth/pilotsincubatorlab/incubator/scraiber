import os
from mimlem import bcf2tiff
from mimlem import simple_stitching
from celery import shared_task
from celery_progress.backend import ProgressRecorder
import sys
import shutil
from scraiber.settings import MEDIA_ROOT
import time




def id_to_bcf_folder(id):
    save_path = os.path.join(MEDIA_ROOT, str(id), 'input')
    return save_path

def id_to_tiff_folder(id):
    save_path = os.path.join(MEDIA_ROOT, str(id), 'tiff')
    return save_path

def id_to_zip_file(id):
    save_path = os.path.join(MEDIA_ROOT, str(id),  'tiffs_from_bcf.zip')
    return save_path


#@shared_task(bind=True)
def bcf2tiff_async(file_path, tiff_save_path):
    progress_recorder = ProgressRecorder(self)
    print("Bcf2tiff id: ", self.request.id)
    progress_recorder.set_progress(0, 1, description='starting converison')
    bcf2tiff.bcf2tiff(file_path, tiff_save_path)
    progress_recorder.set_progress(1, 1, description='finished converison')


@shared_task
def stitching_task(id, num_files):
    transact_path = id_to_tiff_folder(id)

    zip_file = id_to_zip_file(id).split('.')
    ending = zip_file[-1]
    zip_file = '.'.join(zip_file[:-1])
    # shutil.make_archive(zip_file, ending, transact_path)
    if num_files > 1:
        try:
            transact_path = simple_stitching.stitch_auto(transact_path, 0, 0)
            info = "Stitching successful"
        except Exception as e:
            print(e, file=sys.stderr)
            info = "Stitching failed: the tile filenames has to be in `something_(<line nr.>, <column nr.>).bcf` format"
    shutil.make_archive(zip_file, ending, transact_path)
    return info