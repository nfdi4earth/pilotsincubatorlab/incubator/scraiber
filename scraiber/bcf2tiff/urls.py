from django.urls import path

from . import views

app_name = 'bcf2tiff'
urlpatterns = [
    path('', views.UploadFile, name='UploadFile'),
    path('convert/', views.convert, name='convert'),
    path('segment/', views.segment_view, name='segment'),
    path('category/', views.category_view, name='category'),
    path('analysis/', views.analysis_info_from_path),
    path('download/', views.download_file),
    path('thumbnail/', views.thumbnail),
    path('thumbnail_seg/', views.thumbnail_seg),
    path('category/thumbnail_analysis/<slug:oli>', views.thumbnail_analysis),
    path('category/download/', views.download_category),
    path('general/', views.general, name='general'),
    #path('download-tiff/', views.download_tiff),
]

