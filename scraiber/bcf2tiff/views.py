from django.shortcuts import render, HttpResponse, HttpResponseRedirect, reverse
from django.http import JsonResponse
from .forms import InputForm, ConvForm
from mimlem import simple_stitching, segment, bcf2tiff, section_analysis
import mimetypes
from django.http import FileResponse
from bcf2tiff import tasks
import os
from glob import glob
import tifffile
import json
import numpy as np
from django.views.decorators.csrf import csrf_exempt
import time
import shutil
import secrets


def handle_uploaded_file(f, path):
    with open(path, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


def UploadFile(request):
    info = 'Select the files to upload'
    if request.method == 'POST':
        form = InputForm(request.POST,request.FILES)
        files = request._files.values()
        files = [file for file in files]
        print("The files: ", files)
        print("Is valid: ", form.is_valid())

        request.session['transaction_id'] = secrets.token_urlsafe(32)
        save_path = tasks.id_to_bcf_folder(request.session['transaction_id'])
        if not os.path.exists(save_path):
            os.makedirs(save_path)

        convert_files(files, tasks.id_to_tiff_folder(request.session['transaction_id']), request, save_path)
        redirect = HttpResponseRedirect(reverse("bcf2tiff:general"))
        return redirect

    form = InputForm()
    context = {
        'form': form,
        'info': info
    }
    return render(request, 'bcf2tiff/index.html', context)



def convert_files(files, tiff_save_path, request, save_path):
    file_names = []
    print("file names: ", file_names)
    for f in files:
        file_names.append(f._name)
        file_path = os.path.join(save_path, f._name)
        handle_uploaded_file(f, file_path)

        if not os.path.exists(tiff_save_path):
            os.makedirs(tiff_save_path)
        #tasks.bcf2tiff_async(file_path, tiff_save_path)
        bcf2tiff.bcf2tiff(file_path, tiff_save_path)
    request.session['infiles'] = file_names


def convert(request):

    tasks.stitching_task( id =request.session['transaction_id'], num_files=len(request.session['infiles']))
    info = "stitching started"
    form = ConvForm()
    if request.method == 'POST':
        form = ConvForm(request.POST, request.FILES)
    context = {
        'form': form,
        'info': info
     }

    return render(request, 'bcf2tiff/convert.html', context= context)


def general(request):

    context = {

     }

    return render(request, 'bcf2tiff/general.html', context= context)



def analysis_info_from_path(request):
    path = tasks.id_to_tiff_folder(request.session['transaction_id'])
    path = os.path.join(path, 'stitched/analyzed')

    cristal_instances = glob(os.path.join(path, "catalogue_raw/*/*_bse.tiff"), recursive = False)
    cristal_table = []
    for cristal in cristal_instances:
        mask = tifffile.imread(cristal.replace('_bse.tiff', '_mask.tiff')) > 0.33
        print(mask.dtype)
        print(np.unique(mask, return_counts=True))
        size = mask.sum()
        id = cristal.split('/')[-1].replace('_bse.tiff', '')
        print(id)
        type = cristal.split('catalogue_raw')[-1].split('/')[1]
        cristal_table.append({'id': id, 'type': type, 'size': str(size), 'isdownload': "◻", 'custom_type': ""})

    #return JsonResponse(cristal_table)
    return HttpResponse(json.dumps(cristal_table), content_type="application/json")

@csrf_exempt
def download_category(request):
    table = json.loads(request.body)

    path = tasks.id_to_tiff_folder(request.session['transaction_id'])
    path = os.path.join(path, 'stitched/analyzed')
    json_log_path = os.path.join(path, 'download_log')
    if not os.path.exists(json_log_path):
        os.makedirs(json_log_path)
    json_log_path = os.path.join(json_log_path, '{}_{}.json'.format(request.session['transaction_id'], time.time()))
    with open(json_log_path, "w") as file:
        json.dump(table, file)
    tmp_path = os.path.join(path, 'tmp')
    if os.path.exists(tmp_path):
        shutil.rmtree(tmp_path)
    os.makedirs(tmp_path)
    for instance in table:
        if instance['isdownload'] == '✓':
            if not instance['custom_type'] is None and len(instance['custom_type']) > 0:
                type = instance['custom_type']
            else:
                type = instance['type']
            tmp_path_type = os.path.join(tmp_path, type)
            if not os.path.exists(tmp_path_type):
                os.makedirs(tmp_path_type)

            oli_path_jpeg = glob(os.path.join(tasks.id_to_tiff_folder(request.session['transaction_id']),
                                          'stitched/analyzed/catalogue_raw/*/' + instance['id'] + '.jpeg'), recursive=True)[0]
            oli_path_bse = glob(os.path.join(tasks.id_to_tiff_folder(request.session['transaction_id']),
                                         'stitched/analyzed/catalogue_raw/*/' + instance['id'] + '_bse.tiff'), recursive=True)[0]
            oli_path_mask = glob(os.path.join(tasks.id_to_tiff_folder(request.session['transaction_id']),
                                        'stitched/analyzed/catalogue_raw/*/' + instance['id'] + '_mask.tiff'), recursive=True)[0]
            oli_paths = [oli_path_jpeg, oli_path_bse, oli_path_mask]
            for oli_file in oli_paths:
                print(oli_file)
                shutil.copy(oli_file, tmp_path_type+'/')

    filepath = os.path.join(path, 'categories')
    shutil.make_archive(os.path.join(path, 'categories'), 'zip', tmp_path)
    filepath += '.zip'
    print(filepath)
    file = open(filepath, 'rb')

    # Set the mime type
    mime_type, _ = mimetypes.guess_type(filepath)
    print("mime type: ", mime_type)
    # Set the return value of the HttpResponse
    response = FileResponse(file, content_type=mime_type)
    # Set the HTTP header for sending to browser
    #response['Content-Disposition'] = "attachment; filename=%s" % filepath.split('/')[-1]
    # Return the response value
    return response




def category_view(request):
    info = ""
    transact_path = tasks.id_to_tiff_folder(request.session['transaction_id'])
    transact_path = os.path.join(transact_path, 'stitched/')

    for _ in range(1000):
        if os.path.exists(os.path.join(transact_path, 'oli_mask.TIFF')):
            break;
        else:
            time.sleep(5)
    _ = section_analysis.analyze(transact_path)
    context = {'info': info}
    return render(request, 'bcf2tiff/category.html', context=context)



def segment_view(request):
    info = ""
    transact_path = tasks.id_to_tiff_folder(request.session['transaction_id'])
    transact_path = os.path.join(transact_path, 'stitched/')
    stitched_path = os.path.join(transact_path, 'stitched_CH0.TIFF')
    for _ in range(1000):
        if os.path.exists(stitched_path):
            break;
        else:
            time.sleep(5)
    segment.bse(stitched_path, transact_path)


    form = ConvForm()
    if request.method == 'POST':
        form = ConvForm(request.POST, request.FILES)
    context = {
        'form': form,
        'info': info
     }

    return render(request, 'bcf2tiff/segment.html', context= context)



def download_file(request):
    filepath = tasks.id_to_zip_file(request.session['transaction_id'])
    file = open(filepath, 'rb')

    # Set the mime type
    mime_type, _ = mimetypes.guess_type(filepath)
    print("mime type: ", mime_type)
    # Set the return value of the HttpResponse
    response = HttpResponse(file, content_type=mime_type)
    # Set the HTTP header for sending to browser
    response['Content-Disposition'] = "attachment; filename=%s" % filepath.split('/')[-1]
    # Return the response value
    return response


def thumbnail(request):
    thumbnail_path = os.path.join(tasks.id_to_tiff_folder(request.session['transaction_id']), 'stitched/stitched_CH0.png')
    for _ in range(1000):
        if os.path.exists(thumbnail_path):
            break;
        else:
            time.sleep(5)
    response = FileResponse(open(thumbnail_path, "rb"))
    return response


def thumbnail_seg(request):
    thumbnail_path = os.path.join(tasks.id_to_tiff_folder(request.session['transaction_id']), 'stitched/oli_mask.png')
    response = FileResponse(open(thumbnail_path, "rb"))
    return response

def thumbnail_analysis(request, oli):
    thumbnail_path = os.path.join(tasks.id_to_tiff_folder(request.session['transaction_id']), 'stitched/analyzed/catalogue_raw/*/' + oli + '.jpeg')
    thumbnail_path = glob(thumbnail_path, recursive=False)[0]
    response = FileResponse(open(thumbnail_path, "rb"))
    return response


