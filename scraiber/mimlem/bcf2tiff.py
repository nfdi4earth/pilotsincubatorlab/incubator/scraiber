import ntpath
import os.path
import imageio
import hyperspy.api as hs
import argparse
from meta import BCFMetadata


def bcf2tiff(file_path, output_folder):
    print("Load file: {}".format(file_path))
    b = hs.load(file_path)
    b[0].original_folder = ''
    file_name = ntpath.basename(file_path)
    file_name = os.path.splitext(file_name)[0]
    output_file_path_stumb = os.path.join(output_folder, file_name)

    metadata = BCFMetadata.from_bcf(b)
    metadata.save_yaml(output_folder)
    output_file_path = output_file_path_stumb + '_' + 'CH0' + '.TIFF'
    imageio.imwrite(output_file_path, b[0].data)

    for mapping in b[-1].get_lines_intensity():
        output_file_path = output_file_path_stumb + '_' + mapping.metadata.Sample.elements[0] + '.TIFF'
        print('Write file: {} {}'.format(output_file_path, mapping.data.dtype))
        imageio.imwrite(output_file_path, mapping.data)

  
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('files', nargs='+')
    parser.add_argument('-e', nargs=1)

    parser.add_argument('--out', dest='output_folder', default='.')
    args = parser.parse_args()
    for file in args.files:
        bcf2tiff(file, args.output_folder)