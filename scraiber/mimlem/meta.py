import os.path

import yaml
import hyperspy.api as hs


class BCFMetadata:
    def __init__(self, metadata, original_metadata, parameters):
        self.metadata = metadata
        self.original_metadata = original_metadata
        self.parameters = parameters

    def save_yaml(self, folder):
        path = os.path.join(folder, self.parameters['filename'])+'.yaml'
        with open(path, 'w') as f:
            yaml.dump(self, f)

    @classmethod
    def from_bcf(cls, bcf):
        if type(bcf) is str:
            bcf = hs.load(bcf)
        bcf = bcf[0]
        return BCFMetadata(bcf._metadata.as_dictionary(),  bcf._original_metadata.as_dictionary(), bcf.tmp_parameters.as_dictionary())

    @classmethod
    def from_yaml(cls, file_path):
        with open(file_path, 'r') as f:
            rbm = yaml.load(f, Loader=yaml.Loader)
        return rbm

def yaml_constructor( loader, node):
    fields = loader.construct_mapping(node)
    return BCFMetadata(**fields)

yaml.add_constructor(BCFMetadata, yaml_constructor)


if __name__ == "__main__":  # debug the module
    bm = BCFMetadata.from_bcf('/data/geology_mount/mimlem/profiles_and_differentscans/PH2 - Cycles 10, Dwell Time 16, Line Average 10,MP-KL-3-1_2019_06_22_12_15_22_050/Mapping_(3,6).bcf')
    bm.save_yaml('.')
    #with open('test_tmp.yaml', 'w') as f:
    #    yaml.dump(bm, f)

    #with open('test_tmp.yaml', 'r') as f:
    #    rbm = yaml.load(f, Loader=yaml.Loader)
    rbm  = BCFMetadata.from_yaml('test_tmp.yaml')
    print(rbm)
    print(type(rbm))
