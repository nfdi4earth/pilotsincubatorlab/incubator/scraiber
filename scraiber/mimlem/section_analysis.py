import numpy as np
from numba import jit
import imageio, tifffile
from matplotlib import pyplot as plt
from sys import stdout
import pandas as pd
import seaborn as sns
import os
import scipy.ndimage
import argparse

ERODE = 0
DILATE = 1

## Constants for croping the input.
CROP_TOP = 0
CROP_BOTTOM = 0 #CROP_TOP  # symetric for now
CROP_LEFT = 0#1200
CROP_RIGHT = 0#CROP_LEFT
FAC = 1


# MORPHOLOGY

@jit(nopython=True)
def get_structure_element_circle(D):
    if D % 2 == 0:
        D += 1
    cx = D / 2
    cy = D / 2
    d_max = (D / 2) - 0.3

    SE = np.zeros((D, D), dtype=np.bool_)
    for x in range(D):
        for y in range(D):
            xc = x + 0.5
            yc = y + 0.5
            dx = cx - xc
            dy = cy - yc
            dist = (dx ** 2 + dy ** 2) ** 0.5
            if dist <= d_max:
                SE[x, y] = 1

    return SE


@jit(nopython=True)
def get_structure_element_square(N):
    if N % 2 == 0:
        N += 1
    return np.ones((N, N), dtype=np.bool_)


@jit(nopython=True)
def apply_operation(binary_image, structural_element, operation):
    fs = structural_element.shape[0]
    hfs = int(fs / 2)

    h, w = binary_image.shape
    out_image = np.zeros((h, w), dtype=np.bool_)

    for x_a in range(h):
        for y_a in range(w):
            im_x_low = max(0, x_a - hfs)
            im_x_high = min(h, x_a + 1 + hfs)
            im_y_low = max(0, y_a - hfs)
            im_y_high = min(w, y_a + 1 + hfs)
            im_area = binary_image[im_x_low:im_x_high, im_y_low:im_y_high].astype(np.int64)

            se_x_low = max(0, hfs - x_a)
            se_x_high = min(fs, h - x_a + hfs)
            se_y_low = max(0, hfs - y_a)
            se_y_high = min(fs, w - y_a + hfs)
            se_area = structural_element[se_x_low:se_x_high, se_y_low:se_y_high].astype(np.int64)

            if operation == DILATE:
                out_image[x_a, y_a] = np.max(np.logical_and(im_area, se_area))
                # print(np.max(im_area * se_area))
                # out_image[x_a, y_a] = np.max(im_area * se_area)
            elif operation == ERODE:
                # print(np.min(im_area * se_area))
                out_image[x_a, y_a] = 1 - np.max(np.logical_and(1 - im_area, se_area))
                # out_image[x_a, y_a] = np.min(im_area * se_area)

    return out_image


@jit(nopython=True)
def distance_transform_manhattan(binary_image):
    h, w = binary_image.shape

    border_value = 0

    FP = np.zeros((h, w), dtype=np.int64)
    for x in range(h):
        for y in range(w):
            if binary_image[x, y]:
                north_neighbour = border_value
                if x > 0:
                    north_neighbour = 1 + FP[x - 1, y]

                west_neighbour = border_value
                if y > 0:
                    west_neighbour = 1 + FP[x, y - 1]

                FP[x, y] = min(north_neighbour, west_neighbour)

    BP = np.zeros((h, w), dtype=np.int64)
    for x in range(h - 1, -1, -1):
        for y in range(w - 1, -1, -1):
            if binary_image[x, y]:
                south_neighbour = border_value
                if x < h - 1:
                    south_neighbour = 1 + BP[x + 1, y]

                east_neighbour = border_value
                if y < w - 1:
                    east_neighbour = 1 + BP[x, y + 1]

                BP[x, y] = min(FP[x, y], min(south_neighbour, east_neighbour))

    return BP


@jit(nopython=True)
def distance_transform_euclidian(binary_image, N):
    h, w = binary_image.shape

    FP_values = np.zeros((N + 1, N + 1), dtype=np.float32)
    BP_values = np.zeros((N + 1, N + 1), dtype=np.float32)
    FPI_values = np.zeros((N + 1, N + 1), dtype=np.float32)
    BPI_values = np.zeros((N + 1, N + 1), dtype=np.float32)
    for x in range(N + 1):
        for y in range(x, N + 1):
            if x == 0 and y == 0:
                v = h * w
            else:
                v = np.sqrt(x * x + y * y)
            FP_values[N - x, N - y] = v
            BP_values[x, y] = v
            FPI_values[N - x, y] = v
            BPI_values[x, N - y] = v

            if x != y:
                FP_values[N - y, N - x] = v
                BP_values[y, x] = v
                FPI_values[N - y, x] = v
                BPI_values[y, N - x] = v

    # FP_values[N, N] = h * w
    # BP_values[0, 0] = h * w
    #
    # FP_values[0,0] = 0.0
    # FP_values[1,0] = 1.0
    # FP_values[0,1] = 1.0
    # FP_values[1,1] = 1.4

    FP = np.zeros((h + 2 * N, w + 2 * N), dtype=np.float32)
    BP = np.zeros((h + 2 * N, w + 2 * N), dtype=np.float32)
    FPI = np.zeros((h + 2 * N, w + 2 * N), dtype=np.float32)
    BPI = np.zeros((h + 2 * N, w + 2 * N), dtype=np.float32)

    for x in range(N, N + h):
        for y in range(N, N + w):
            if binary_image[x - N, y - N]:
                neighbours = FP[x - N:x + 1, y - N:y + 1]
                FP[x, y] = np.min(neighbours + FP_values)

    for x in range(h + N - 1, N - 1, -1):
        for y in range(w + N - 1, N - 1, -1):
            if binary_image[x - N, y - N]:
                neighbours = BP[x:x + 1 + N, y:y + 1 + N]
                BP[x, y] = min(FP[x, y], np.min(neighbours + BP_values))

    for x in range(N, N + h):
        for y in range(w + N - 1, N - 1, -1):
            if binary_image[x - N, y - N]:
                neighbours = FPI[x - N:x + 1, y:y + 1 + N]
                FPI[x, y] = min(BP[x, y], np.min(neighbours + FPI_values))

    for x in range(h + N - 1, N - 1, -1):
        for y in range(N, N + w):
            if binary_image[x - N, y - N]:
                neighbours = BPI[x:x + 1 + N, y - N:y + 1]
                BPI[x, y] = min(FPI[x, y], np.min(neighbours + BPI_values))

    return BPI[N:N + h, N:N + w]


@jit(nopython=True)
def get_connected_components(I):
    """Computes a segment map from connected components (by value).

        Parameters
        ----------
        I : ndarray of bool
            2D array representation of an image

        Returns
        -------
        out : ndarray of int64
            2D array where each entry corresponds to the component ID

        Notes
        -----
        Function searches for connected regions in an image with the same values!
        Function accepts single or multi channel images. Segment IDs will start with zero!
    """

    h, w = I.shape

    ### MAP COORDINATES TO HORIZONTAL LABEL
    label_map_horizontal = np.zeros((h, w), dtype=np.int64)
    next_label = 1
    for x in range(h):
        for y in range(w):
            merge_h = y > 0
            if merge_h:
                if I[x, y - 1] != I[x, y]:
                    merge_h = False
            if merge_h:
                label_map_horizontal[x, y] = label_map_horizontal[x, y - 1]
            else:
                merge_v = x > 0
                if merge_v:
                    if I[x - 1, y] != I[x, y]:
                        merge_v = False
                if merge_v:
                    label_map_horizontal[x, y] = label_map_horizontal[x - 1, y]
                else:
                    label_map_horizontal[x, y] = next_label
                    next_label += 1

    ### MAP HORIZONTAL INDICES TO FINAL INDICES
    final_labels_length = next_label
    final_labels = np.ones((final_labels_length), dtype=np.int64) * -1
    next_final_label = 0
    for x in range(h):
        for y in range(w):
            try_merge = x > 0
            h_label = label_map_horizontal[x, y]
            merge = try_merge
            if try_merge:
                upper_h_label = label_map_horizontal[x - 1, y]
                if h_label == upper_h_label or final_labels[h_label] == final_labels[upper_h_label]:
                    merge = False
                else:
                    if I[x - 1, y] != I[x, y]:
                        merge = False
                if merge:
                    if final_labels[h_label] == -1:
                        final_labels[h_label] = final_labels[upper_h_label]
                    elif final_labels[upper_h_label] != final_labels[h_label]:
                        to_change_final = final_labels[h_label]
                        for i in range(final_labels_length):
                            if final_labels[i] == to_change_final:
                                final_labels[i] = final_labels[upper_h_label]
            if not merge and final_labels[h_label] == -1:
                final_labels[h_label] = next_final_label
                next_final_label += 1

    ### MAP FINAL INDICES TO INDICES WITHOUT GAPS (STARTING FROM ZERO)
    no_gap_labels_length = next_final_label
    no_gap_labels = np.ones((no_gap_labels_length), dtype=np.int64) * -1
    no_gap_index = 0
    for x in range(h):
        for y in range(w):
            if no_gap_labels[final_labels[label_map_horizontal[x, y]]] == -1:
                no_gap_labels[final_labels[label_map_horizontal[x, y]]] = no_gap_index
                no_gap_index += 1

    label_map = np.zeros((h, w), dtype=np.int64)
    for x in range(h):
        for y in range(w):
            label_map[x, y] = no_gap_labels[final_labels[label_map_horizontal[x, y]]]

    return label_map


@jit(nopython=True)
def get_contour_image(S):
    """Computes a contour ID map from a segment map.

        Parameters
        ----------
        S : ndarray of int64
            2D array representing the segment map

        Returns
        -------
        out : ndarray of int64
            2D array representing the contour map

        Notes
        -----
        Each entry of the contour map corresponds to the component ID,
        if the entry it is on the contour of that component, else -1.
        The contour map is computed via contour tracing.
    """

    h, w = S.shape
    num_s = np.max(S) + 1
    C = np.ones((h, w), dtype=np.int64) * -1

    for s in range(num_s):

        # GET CONTOUR SEED FOR CLASS 'c'
        start_pixel = (-1, -1)
        for x in range(h):
            for y in range(w):
                if S[x, y] == s:
                    start_pixel = (x, y)
                    break
            if start_pixel[0] >= 0:
                break

        if start_pixel == (-1, -1):
            continue

        # FOLLOW CONTOUR OF SEGMENT 's'
        search_dir = 0
        current_pixel = start_pixel
        done = False
        while True:
            cpx, cpy = current_pixel
            C[cpx, cpy] = s
            for i in range(8):  # while(True) failes with jit..
                if search_dir == 0 and cpx > 0 and S[cpx - 1, cpy] == s:  # start at pixel above
                    current_pixel = (cpx - 1, cpy)
                    break
                elif search_dir == 45 and cpx > 0 and cpy < w - 1 and S[cpx - 1, cpy + 1] == s:
                    current_pixel = (cpx - 1, cpy + 1)
                    break
                elif search_dir == 90 and cpy < w - 1 and S[cpx, cpy + 1] == s:
                    current_pixel = (cpx, cpy + 1)
                    break
                elif search_dir == 135 and cpx < h - 1 and cpy < w - 1 and S[cpx + 1, cpy + 1] == s:
                    current_pixel = (cpx + 1, cpy + 1)
                    break
                elif search_dir == 180 and cpx < h - 1 and S[cpx + 1, cpy] == s:
                    current_pixel = (cpx + 1, cpy)
                    break
                elif search_dir == 225 and cpx < h - 1 and cpy > 0 and S[cpx + 1, cpy - 1] == s:
                    current_pixel = (cpx + 1, cpy - 1)
                    break
                elif search_dir == 270 and cpy > 0 and S[cpx, cpy - 1] == s:
                    current_pixel = (cpx, cpy - 1)
                    break
                elif search_dir == 315 and cpx > 0 and cpy > 0 and S[cpx - 1, cpy - 1] == s:
                    current_pixel = (cpx - 1, cpy - 1)
                    break
                search_dir = (search_dir + 45) % 360
                if search_dir == 0 and start_pixel == current_pixel:
                    done = True
                    break
            search_dir = (search_dir + 270) % 360

            if done:
                break
    return C


@jit(nopython=True)
def get_saturated_color(hue):
    c = 1.0
    x = 1 - abs((hue / 60) % 2 - 1)
    if hue < 60:
        r_, g_, b_ = c, x, 0
    elif hue < 120:
        r_, g_, b_ = x, c, 0
    elif hue < 180:
        r_, g_, b_ = 0, c, x
    elif hue < 240:
        r_, g_, b_ = 0, x, c
    elif hue < 300:
        r_, g_, b_ = x, 0, c
    else:
        r_, g_, b_ = c, 0, x

    return np.array([r_, g_, b_], dtype=np.float32) * 255


@jit(nopython=True)
def get_saturated_colors(num_colors):
    colors = np.zeros((num_colors, 3), dtype=np.float64)
    for i in range(num_colors):
        if i == 0:
            colors[i, :] = np.ones(3, dtype=np.float64) * 255
        else:
            hue_i = 57 * i
            colors[i, :] = get_saturated_color(hue_i % 360) * (np.sin(i) / 4 + 0.75)
    return colors


@jit(nopython=True)
def label_map2label_image(label_map):
    h, w = label_map.shape
    label_image = np.zeros((h, w, 3), dtype=np.float64)
    num_labels = np.max(label_map) + 1

    colors = get_saturated_colors(num_labels)

    for x in range(h):
        for y in range(w):
            label = label_map[x, y]
            label_image[x, y, :] = colors[label, :]

    return label_image


def analyse_map(M, BSE, Mgn, fac, PIMG, output):
    Se = np.ones((5, 5), dtype=np.bool)
    phaeno_min_pixels = 10000 // (fac * fac)
    subphaeno_min_pixels = 1000 // (fac * fac)
    microlit_min_pixels = 100 // (fac * fac)

    # df = pd.DataFrame(columns=["Type","Size","Mean Mgnr"])

    out_bsebg = np.copy(np.dstack([BSE, ] * 3)).astype(np.float32)
    out_bsebg -= np.min(out_bsebg)
    out_bsebg /= np.max(out_bsebg)
    out_bsebg = np.clip(out_bsebg * 350, 0, 255).astype(np.ubyte)
    out_borders = np.zeros_like(out_bsebg)

    h, w = M.shape
    n = h * w
    print(f"size of map is {h} x {w} = {n} pixels")
    count_true = np.sum(M)
    count_false = n - count_true
    print(f"{count_true} pixels ({int(count_true / n * 100)}%) are foreground")
    print(f"{count_false} pixels ({int(count_false / n * 100)}%) are background")
    full_section = os.path.join(output, 'full_section')
    if not os.path.isfile(os.path.join(full_section,"L-crop.TIFF")):
        print('computing connected components', end='', flush=True)
        L = get_connected_components(M).astype(np.int32)
        L[np.logical_not(M)] = -1
        print('\rcomputing connected components .', end='', flush=True)
        C = get_contour_image(L).astype(np.int32)
        print('\rcomputing connected components ..', end='', flush=True)
        D = distance_transform_euclidian(M, 5)
        print('\rcomputing connected components ... done')
        full_section = 'full_section'
        if not os.path.exists(full_section):
            os.mkdir(full_section)
        tifffile.imwrite(os.path.join(full_section, 'L-crop.TIFF'), L)
        tifffile.imwrite(os.path.join(full_section, 'C-crop.TIFF'), C)
        tifffile.imwrite(os.path.join(full_section, 'D-crop.TIFF'), D)
    else:
        print('using stored LCD')
        L = tifffile.imread(os.path.join(full_section, 'L-crop.TIFF'))
        C = tifffile.imread(os.path.join(full_section, 'C-crop.TIFF'))
        D = tifffile.imread(os.path.join(full_section, 'D-crop.TIFF'))

    print(np.min(L))

    # imageio.imwrite('E-L.jpg', (label_map2label_image(L)*-1 + 255).astype(np.ubyte))
    # imageio.imwrite('E-D.jpg', (D/np.max(D)*255).astype(np.ubyte))

    n_segs = np.max(L) + 1
    print(n_segs)
    unique_ids, counts = np.unique(L, return_counts=True)
    bg_id = unique_ids[np.argmax(counts)]
    print("ID of BG-segment:", bg_id)
    print('pre-classifying instances')

    microlit_ids, microlit_pxcnt = [], []
    subphaeno_ids, subphaeno_pxcnt = [], []
    phaeno_ids, phaeno_pxcnt = [], []

    indx = 0
    summary = np.zeros((len(unique_ids), 7), dtype=np.float)  # row: ID, size, type, x_min, x_max, y_min, y_max
    for id, size in zip(unique_ids, counts):
        if id == -1: continue
        if size > microlit_min_pixels:
            save_boundaries = True

            if size < subphaeno_min_pixels:
                save_boundaries = False
                summary[indx, 2] = 0
                microlit_ids.append(id)
                microlit_pxcnt.append(size)

            elif size < phaeno_min_pixels:
                summary[indx, 2] = 1
                subphaeno_ids.append(id)
                subphaeno_pxcnt.append(size)

            else:
                summary[indx, 2] = -1
                phaeno_ids.append(id)
                phaeno_pxcnt.append(size)

            if save_boundaries:
                where = np.argwhere(L == id)
                summary[indx, 3] = np.min(where[:, 0])
                summary[indx, 4] = np.max(where[:, 0])
                summary[indx, 5] = np.min(where[:, 1])
                summary[indx, 6] = np.max(where[:, 1])

            summary[indx, 0] = id
            summary[indx, 1] = size * fac * fac
            indx += 1

            if not indx % 100:
                print(f'\r{indx} done', end='', flush=True)
    summary = summary[:indx, :]

    print('\rpre-classifying instances  ... done')
    print(f"Got {indx} crystals: {len(microlit_ids)} micro, {len(subphaeno_ids)} sub, {len(phaeno_ids)} pha")

    index = 0
    microlit_data = np.zeros((np.sum(np.array(microlit_pxcnt)), 2))
    if PIMG: borders = np.zeros(C.shape, dtype=np.bool)
    for id, s in zip(microlit_ids, microlit_pxcnt):
        if PIMG: borders = np.logical_or(borders, C == id)
        Li = L == id
        microlit_data[index:index + s, 0] = Mgn[Li]
        microlit_data[index:index + s, 1] = id
        index += s

    if PIMG: borders = scipy.ndimage.binary_dilation(borders, Se)
    if PIMG: out_borders[borders] = [0, 0, 255]
    print("Microlits done..")

    index = 0
    subphaeno_data = np.zeros((int(np.sum(np.array(subphaeno_pxcnt))), 2))
    if PIMG: borders[:, :] = False
    for id, s in zip(subphaeno_ids, subphaeno_pxcnt):
        if PIMG: borders = np.logical_or(borders, C == id)
        Li = L == id
        subphaeno_data[index:index + s, 0] = Mgn[Li]
        subphaeno_data[index:index + s, 1] = id
        index += s
    if PIMG: borders = scipy.ndimage.binary_dilation(borders, Se)
    if PIMG: out_borders[borders] = [255, 0, 200]
    print("Subphaenocrystals done..")

    index = 0
    phaeno_data = np.zeros((np.sum(np.array(phaeno_pxcnt)), 3))
    for id, s in zip(phaeno_ids, phaeno_pxcnt):
        Li = L == id
        Di = D * Li
        if PIMG: Ci = C == id
        if PIMG: Ci = scipy.ndimage.binary_dilation(Ci, Se)

        phaeno_data[index:index + s, 0] = Mgn[Li]
        phaeno_data[index:index + s, 1] = id

        max_distance = np.max(Di)
        if max_distance == 0.0:
            phaeno_data[index:index + s, 2] = -1
        else:
            intensity_core = np.median(BSE[Di > max_distance / 2])
            intensity_rim = np.median(BSE[np.logical_and(Di <= max_distance / 2, Di > 10)])
            print(f"Phaeno segment: {id}, size: {s} px2, C/R: {intensity_core / intensity_rim}")

            r = intensity_core / intensity_rim
            if np.isnan(r): r = 1.0

            if r > 1.04:  # rev zoning
                c = [255, 0, 0]
                phaeno_data[index:index + s, 2] = 2
                summary[int(np.where(summary[:, 0] == id)[0]), 2] = 2
            elif r < 0.96:  # reg zoning
                c = [255, 255, 0]
                phaeno_data[index:index + s, 2] = 3
                summary[int(np.where(summary[:, 0] == id)[0]), 2] = 3
            else:  # no zoning
                c = [255, 255, 255]
                phaeno_data[index:index + s, 2] = 4
                summary[int(np.where(summary[:, 0] == id)[0]), 2] = 4
            if PIMG: out_borders[Ci] = c

        index += s

    print("Phaenocrystals done..")

    data = (microlit_data, subphaeno_data, phaeno_data, summary)

    # if len(phaeno_ids) > 0:
    # print(f"{bright_cores} ({int(100*bright_cores/len(phaeno_ids))}%) have a brighter core")
    # plt.figure(1)
    # plt.imshow(T)

    # bins = min(sizes) + 1.8 ** (np.arange(0, 20))
    # plt.xscale('log')
    # plt.hist(sizes, bins=bins)
    # plt.figure(2)
    # plt.hist(sizes)

    W = np.sum(out_borders, 2) == 0
    bg_and_borders = np.copy(out_borders)
    bg_and_borders[W] = out_bsebg[W]

    return out_borders, bg_and_borders, data


def kde_plot_all():
    for i in [5, 6, 7, 8, 9, 11, 17, 18, 23, 24, 28, 29, 32, 33]:
        data = np.load(f"section_analysis_results/{i}_section-data.npy", allow_pickle=True)
        (_, microlit_mgn_all, _, subphaeno_mgn_all, _, phaeno_mgn_all,
         _, phaenoD_mgn_all, _, phaenoR_mgn_all, _, phaenoN_mgn_all) = data

    for i in [5, 6, 7, 8, 9, 11, 17, 18, 23, 24, 28, 29, 32, 33]:
        data = np.load(f"section_analysis_results/{i}_section-data.npy", allow_pickle=True)
        (_, microlit_mgn, _, subphaeno_mgn, _, phaeno_mgn,
         _, phaenoD_mgn, _, phaenoR_mgn, _, phaenoN_mgn) = data

        microlit_mgn_all += microlit_mgn
        subphaeno_mgn_all += subphaeno_mgn
        phaeno_mgn_all += phaeno_mgn
        phaenoD_mgn_all += phaenoD_mgn
        phaenoR_mgn_all += phaenoR_mgn
        phaenoN_mgn_all += phaenoN_mgn

    df = pd.DataFrame(columns=["Type", "Mg#"])

    micro = np.repeat(np.array((("Microlit", 0),)), (len(microlit_mgn_all)), axis=0)
    micro[:, 1] = microlit_mgn_all
    df = df.append(pd.DataFrame(micro, columns=["Type", "Mg#"]), ignore_index=True)

    subp = np.repeat(np.array((("Micro Phaenocryst", 0),)), (len(subphaeno_mgn_all)), axis=0)
    subp[:, 1] = subphaeno_mgn_all
    df = df.append(pd.DataFrame(subp, columns=["Type", "Mg#"]), ignore_index=True)

    phaD = np.repeat(np.array((("Phaenocryst (reg. zoning)", 0),)), (len(phaenoD_mgn_all)), axis=0)
    phaD[:, 1] = phaenoD_mgn_all
    df = df.append(pd.DataFrame(phaD, columns=["Type", "Mg#"]), ignore_index=True)

    phaR = np.repeat(np.array((("Phaenocryst (rev. zoning)", 0),)), (len(phaenoR_mgn_all)), axis=0)
    phaR[:, 1] = phaenoR_mgn_all
    df = df.append(pd.DataFrame(phaR, columns=["Type", "Mg#"]), ignore_index=True)

    phaN = np.repeat(np.array((("Phaenocryst (no zoning)", 0),)), (len(phaenoN_mgn_all)), axis=0)
    phaN[:, 1] = phaenoN_mgn_all
    df = df.append(pd.DataFrame(phaN, columns=["Type", "Mg#"]), ignore_index=True)

    df = df.astype({"Type": str, "Mg#": float})
    print(df.head())

    sns.kdeplot(data=df, x="Mg#", hue="Type", common_norm=False, common_grid=True, bw_adjust=0.7, )
    plt.savefig(f'section_analysis_results/all-kde.png')
    plt.clf()

    # plt.hist(microlit_mgn,)
    # plt.show()


def kde_plots():
    for i in [5, 6, 7, 8, 9, 11, 17, 18, 23, 24, 28, 29, 32, 33]:
        data = np.load(f"section_analysis_results/{i}_section-data.npy", allow_pickle=True)
        (microlit_sizes, microlit_mgn, subphaeno_sizes, subphaeno_mgn, phaeno_sizes, phaeno_mgn,
         phaenoD_sizes, phaenoD_mgn, phaenoR_sizes, phaenoR_mgn, phaenoN_sizes, phaenoN_mgn) = data
        df = pd.DataFrame(columns=["Type", "Mg#"])

        micro = np.repeat(np.array((("Microlit", 0),)), (len(microlit_mgn)), axis=0)
        micro[:, 1] = microlit_mgn
        df = df.append(pd.DataFrame(micro, columns=["Type", "Mg#"]), ignore_index=True)

        subp = np.repeat(np.array((("Micro Phaenocryst", 0),)), (len(subphaeno_mgn)), axis=0)
        subp[:, 1] = subphaeno_mgn
        df = df.append(pd.DataFrame(subp, columns=["Type", "Mg#"]), ignore_index=True)

        phaD = np.repeat(np.array((("Phaenocryst (reg. zoning)", 0),)), (len(phaenoD_mgn)), axis=0)
        phaD[:, 1] = phaenoD_mgn
        df = df.append(pd.DataFrame(phaD, columns=["Type", "Mg#"]), ignore_index=True)

        phaR = np.repeat(np.array((("Phaenocryst (rev. zoning)", 0),)), (len(phaenoR_mgn)), axis=0)
        phaR[:, 1] = phaenoR_mgn
        df = df.append(pd.DataFrame(phaR, columns=["Type", "Mg#"]), ignore_index=True)

        phaN = np.repeat(np.array((("Phaenocryst (no zoning)", 0),)), (len(phaenoN_mgn)), axis=0)
        phaN[:, 1] = phaenoN_mgn
        df = df.append(pd.DataFrame(phaN, columns=["Type", "Mg#"]), ignore_index=True)

        df = df.astype({"Type": str, "Mg#": float})
        print(df.head())

        sns.kdeplot(data=df, x="Mg#", hue="Type", common_norm=False, common_grid=True, bw_adjust=0.7, )
        plt.savefig(f'section_analysis_results/{i}-kde.png')
        plt.clf()

        # plt.hist(microlit_mgn,)
        # plt.show()


def analyze_sections():
    # Load olivine map and bse image

    # ols = "./testdata/Mapping_(9,9)_ols.TIFF"  # path to binary map
    # bse = "./testdata/Mapping_(9,9)_CH0.TIFF"  # path to binary map
    # I = tifffile.imread(ols) if ols.lower().endswith("tiff") else imageio.imread(ols)
    # B = tifffile.imread(bse) if ols.lower().endswith("tiff") else imageio.imread(bse)
    # map = I > 0  # Convert to binary image
    # for i in [5, 6, 7, 8, 9, 11, 17,18, 23, 24, 28, 29, 32, 33]:
    for i in [6, 7, 8, 9, 11, 17, 18, 23, 24, 28, 29, 32, 33]:
        # for i in [17, ]:
        ols = f"./data/stiched_sections/{i}_section/phdl_binseg_prob.npy"  # path to binary map
        # ols = f"./data/stiched_sections/{i}_section/phth_binseg.npy"  # path to binary map
        bse = f"./data/stiched_sections/{i}_section/ph_CH0.TIFF"  # path to binary map
        mgr = f"./data/stiched_sections/MG_Nr/{i}_section/ph_pred_MG_Nr.TIFF"  # path to binary map
        I = np.load(ols)
        B = tifffile.imread(bse) if bse.lower().endswith("tiff") else imageio.imread(bse)
        N = tifffile.imread(mgr) if mgr.lower().endswith("tiff") else imageio.imread(mgr)

        print(N.max(), N.min())

        # I = I[:2744:, 3000:-400]
        # B = B[:2744:, 3000:-400]
        # N = N[:2744:, 3000:-400]
        map = I > 0.1  # Convert to binary image
        print(B.shape, B.min(), B.max())

        # Opening and Closing

        print('doing morphology magic ', end='', flush=True)
        SE = get_structure_element_circle(5)
        SE2 = get_structure_element_circle(9)
        # map = apply_operation(map, SE, 0)
        print('\rdoing morphology magic .', end='', flush=True)
        # map = apply_operation(map, SE2, 1)
        print('\rdoing morphology magic ..', end='', flush=True)
        ##map = apply_operation(map, SE, 1)
        # map = apply_operation(map, SE, 0)
        print('\rdoing morphology magic ... done')

        # previews

        # plt.figure(0)
        # plt.subplot(1, 3, 1)
        # plt.imshow(B)
        # plt.subplot(1, 3, 2)
        # plt.imshow(B * map.astype(np.float))
        # plt.subplot(1, 3, 3)
        # D = distance_transform_euclidian(map, 5)
        # plt.imshow(D)
        # imageio.imwrite('E-mgr.jpg', np.clip(N / 150, 0, 255).astype(np.ubyte))
        # imageio.imwrite('E-mgr-binary.jpg', (np.clip(N / 150, 0, 255) * map).astype(np.ubyte))
        # imageio.imwrite('E-binary.jpg', (map * 255).astype(np.ubyte))

        out, out2, data = analyse_map(map, B, N)

        np.save(f'section_analysis_results/{ols.split("/")[-2]}-data.npy', data)
        imageio.imwrite(f'section_analysis_results/CL-{ols.split("/")[-2]}.jpg', out)
        imageio.imwrite(f'section_analysis_results/CL-{ols.split("/")[-2]}-clean.jpg', out2)
        # np.save('E-data.npy', data)
        # imageio.imwrite('E-iso.jpg', out)
        # imageio.imwrite('E-clean.jpg', out2)


def analyze_full(ols, bse, output, mgr):
    PIMG = False
    fac = FAC
    # Load olivine map and bse image

    # ols = "./testdata/Mapping_(9,9)_ols.TIFF"  # path to binary map
    # bse = "./testdata/Mapping_(9,9)_CH0.TIFF"  # path to binary map
    # I = tifffile.imread(ols) if ols.lower().endswith("tiff") else imageio.imread(ols)
    # B = tifffile.imread(bse) if ols.lower().endswith("tiff") else imageio.imread(bse)
    # map = I > 0  # Convert to binary image

    I = tifffile.imread(ols) if ols.lower().endswith("TIFF") else imageio.imread(ols)
    B = tifffile.imread(bse) if bse.lower().endswith("TIFF") else imageio.imread(bse)
    print(B.shape)
    if os.path.exists(mgr):
        N = tifffile.imread(mgr) if mgr.lower().endswith("TIFF") else imageio.imread(mgr)
    else:
        '''
        a = -7.4214
        b = 1.0059
        N = B * a + b
        tifffile.imwrite(mgr, N)
        '''
        N = np.zeros(B.shape)

    I = I[CROP_TOP:-CROP_BOTTOM or None:fac, CROP_LEFT:-CROP_RIGHT or None:fac]
    B = B[CROP_TOP:-CROP_BOTTOM or None:fac, CROP_LEFT:-CROP_RIGHT or None:fac]
    N = N[CROP_TOP:-CROP_BOTTOM or None:fac, CROP_LEFT:-CROP_RIGHT or None:fac]
    print(N.max(), N.min())

    map = I > 0.1  # Convert to binary image
    print(B.shape, B.min(), B.max())
    '''
    # Opening and Closing
    print('doing morphology magic ', end='', flush=True)
    SE = get_structure_element_circle(5)
    SE2 = get_structure_element_circle(9)
    # map = apply_operation(map, SE, 0)
    print('\rdoing morphology magic .', end='', flush=True)
    # map = apply_operation(map, SE2, 1)
    print('\rdoing morphology magic ..', end='', flush=True)
    ##map = apply_operation(map, SE, 1)
    # map = apply_operation(map, SE, 0)
    print('\rdoing morphology magic ... done')
    '''


    out_borders, bg_and_borders, data = analyse_map(map, B, N, fac, PIMG, output)
    microlit_data, subphaeno_data, phaeno_data, summary = data
    full_analysis_results = os.path.join(output, 'full_analysis_results')
    if not os.path.exists(full_analysis_results):
        os.makedirs(full_analysis_results)
    df = pd.DataFrame(data=microlit_data, columns=["mgnr", "c-id"])
    df = df.astype({"mgnr": float, "c-id": int})
    df.to_csv(f'{full_analysis_results}/microlit_data.csv', float_format='%.3f')

    df = pd.DataFrame(data=subphaeno_data, columns=["mgnr", "c-id"])
    df = df.astype({"mgnr": float, "c-id": int})
    df.to_csv(f'{full_analysis_results}/subphaeno_data.csv', float_format='%.3f')

    df = pd.DataFrame(data=phaeno_data, columns=["mgnr", "c-id", "c-type"])
    df = df.astype({"mgnr": float, "c-id": int, "c-type": int})
    df.to_csv(f'{full_analysis_results}/phaeno_data.csv', float_format='%.3f')

    df = pd.DataFrame(data=summary, columns=["c-id", "metric_size", "c-type", "x_min", "x_max", "y_min", "y_max"])
    df = df.astype(
        {"c-id": int, "metric_size": int, "c-type": int, "x_min": int, "x_max": int, "y_min": int, "y_max": int})
    df.to_csv(f'{full_analysis_results}/crystal_summary.csv')

    if PIMG: tifffile.imwrite(f'{full_analysis_results}/CL-{ols.split("/")[-2]}-borders_only-cropped.tiff', out_borders)
    if PIMG: imageio.imwrite(f'{full_analysis_results}/CL-{ols.split("/")[-2]}-borders-cropped.jpg', bg_and_borders)
    if PIMG: tifffile.imwrite(f'{full_analysis_results}/CL-{ols.split("/")[-2]}-borders-cropped.tiff', bg_and_borders)


def kde_plot_full():
    microlits = pd.read_csv('full_analysis_results/microlit_data_int.csv')
    microlits["c-type"] = "Microlit"

    subphaenos = pd.read_csv('full_analysis_results/subphaeno_data_int.csv')
    subphaenos["c-type"] = "Subphenocryst"

    phaenos = pd.read_csv('full_analysis_results/phaeno_data_int.csv')
    phaenos["c-type"][phaenos["c-type"] == 2] = "Phenocryst (rev. zoning)"
    phaenos["c-type"][phaenos["c-type"] == 3] = "Phenocryst (reg. zoning)"
    phaenos["c-type"][phaenos["c-type"] == 4] = "Phenocryst (no zoning)"

    df = microlits.append(subphaenos, ignore_index=True).append(phaenos, ignore_index=True)

    # microlits = pd.read_csv('full_analysis_results/microlit_data_int.csv')
    # microlits["c-type"] = "All"
    # subphaenos = pd.read_csv('full_analysis_results/subphaeno_data_int.csv')
    # subphaenos["c-type"] = "All"
    # phaenos = pd.read_csv('full_analysis_results/phaeno_data_int.csv')
    # phaenos["c-type"] = "All"
    #
    # df_all = microlits.append(subphaenos,ignore_index=True).append(phaenos,ignore_index=True)
    # df = df.append(df_all, ignore_index=True)

    df = df.astype({"c-type": str, "mgnr": float})
    df = df.rename(columns={"c-type": "Type", "mgnr": "Mg#"})
    print(df.head())

    # sns.kdeplot(data=df, x="Mg#", hue="Type", common_norm=False, bw_adjust=1.0, )#.7
    # plt.show()

    # sns.kdeplot(data=df, x="Mg#", hue="Type", common_norm=False, bw_adjust=0.75, )#.7
    # plt.show()

    sns.kdeplot(data=df, x="Mg#", hue="Type", common_norm=False, bw_adjust=0.5, )  # .7
    plt.show()


def swarm_plot_full():
    N = -1
    N_red = -1
    data = np.load(f"full_analysis_results/full_section-data.npy", allow_pickle=True)
    (microlit_sizes, microlit_mgn, subphaeno_sizes, subphaeno_mgn, phaeno_sizes, phaeno_mgn,
     phaenoD_sizes, phaenoD_mgn, phaenoR_sizes, phaenoR_mgn, phaenoN_sizes, phaenoN_mgn) = data
    df = pd.DataFrame(columns=["Type", "Size"])
    df_red = pd.DataFrame(columns=["Type", "Size"])

    print("# of Microlits: ", len(microlit_sizes))
    micro = np.repeat(np.array((("Microlit", 0),)), (len(microlit_sizes)), axis=0)
    micro[:, 1] = microlit_sizes
    df = df.append(pd.DataFrame(micro[:N], columns=["Type", "Size"]), ignore_index=True)
    df_red = df_red.append(pd.DataFrame(micro[:250], columns=["Type", "Size"]), ignore_index=True)

    print("# of Micro Phaenocryst: ", len(subphaeno_sizes))
    subp = np.repeat(np.array((("Micro Phaenocryst", 0),)), (len(subphaeno_sizes)), axis=0)
    subp[:, 1] = subphaeno_sizes
    df = df.append(pd.DataFrame(subp[:N], columns=["Type", "Size"]), ignore_index=True)
    df_red = df_red.append(pd.DataFrame(subp[:250], columns=["Type", "Size"]), ignore_index=True)

    print("# of Phaenocryst (reg. zoning): ", len(phaenoD_sizes))
    phaD = np.repeat(np.array((("Phaenocryst (reg. zoning)", 0),)), (len(phaenoD_sizes)), axis=0)
    phaD[:, 1] = phaenoD_sizes
    df = df.append(pd.DataFrame(phaD[:N], columns=["Type", "Size"]), ignore_index=True)
    df_red = df_red.append(pd.DataFrame(phaD, columns=["Type", "Size"]), ignore_index=True)

    print("# of Phaenocryst (rev. zoning) ", len(phaenoR_sizes))
    phaR = np.repeat(np.array((("Phaenocryst (rev. zoning)", 0),)), (len(phaenoR_sizes)), axis=0)
    phaR[:, 1] = phaenoR_sizes
    df = df.append(pd.DataFrame(phaR, columns=["Type", "Size"]), ignore_index=True)
    df_red = df_red.append(pd.DataFrame(phaR, columns=["Type", "Size"]), ignore_index=True)

    print("# of Phaenocryst (no zoning) ", len(phaenoN_sizes))
    phaN = np.repeat(np.array((("Phaenocryst (no zoning)", 0),)), (len(phaenoN_sizes)), axis=0)
    phaN[:, 1] = phaenoN_sizes
    df = df.append(pd.DataFrame(phaN[:N], columns=["Type", "Size"]), ignore_index=True)
    df_red = df_red.append(pd.DataFrame(phaN[:250], columns=["Type", "Size"]), ignore_index=True)

    df = df.astype({"Type": str, "Size": float})
    df_red = df_red.astype({"Type": str, "Size": float})
    df['Size'] = np.clip(df['Size'] * 4, 0, np.inf)
    df_red['Size'] = np.clip(df_red['Size'] * 4, 0, np.inf)
    df.rename(columns={'Size': 'Size [µm²]'}, inplace=True)
    df_red.rename(columns={'Size': 'Size [µm²]'}, inplace=True)

    print(df.head())
    df.to_csv('full_analysis_results/Sizes.csv')
    sns.set_theme(style="whitegrid")

    ax = sns.boxplot(data=df, y="Type", x="Size [µm²]", whis=np.inf)
    ax.set(xscale="log")
    # sns.violinplot(data=df, y="Type", x="Size [µm²]")
    ax = sns.swarmplot(data=df_red, y="Type", x="Size [µm²]", size=2, color=".2")
    ax.set(xscale="log")
    plt.savefig(f'full_analysis_results/full-swarm.png')
    plt.show()


def methodic_visualization():
    fac = FAC
    I = "./data/full_section/data_dlols.TIFF"  # path to binary map
    B = "./data/full_section/data_CH0.TIFF"  # path to binary map
    N = "./data/full_section/data_pred_MG_Nr.TIFF"  # path to binary map
    C = "./data/full_section/C-crop.TIFF"  # path to binary map
    D = "./data/full_section/D-crop.TIFF"  # path to binary map
    L = "./data/full_section/L-crop.TIFF"  # path to binary map

    I = tifffile.imread(I) if I.lower().endswith("tiff") else imageio.imread(I)
    B = tifffile.imread(B) if B.lower().endswith("tiff") else imageio.imread(B)
    N = tifffile.imread(N) if N.lower().endswith("tiff") else imageio.imread(N)
    C = tifffile.imread(C) if C.lower().endswith("tiff") else imageio.imread(C)
    D = tifffile.imread(D) if D.lower().endswith("tiff") else imageio.imread(D)
    L = tifffile.imread(L) if L.lower().endswith("tiff") else imageio.imread(L)

    I = I[CROP_TOP:-CROP_BOTTOM or None:fac, CROP_LEFT:-CROP_RIGHT or None:fac]
    B = B[CROP_TOP:-CROP_BOTTOM or None:fac, CROP_LEFT:-CROP_RIGHT or None:fac]
    N = N[CROP_TOP:-CROP_BOTTOM or None:fac, CROP_LEFT:-CROP_RIGHT or None:fac]

    print('reading done')

    out = "./mimlem_metho_visu/x.png"


def make_cataloge():
    fac = 1
    coords_fac = FAC

    ols = "./data/full_section/data_dlols.TIFF"  # path to binary map
    bse = "./data/full_section/data_CH0.TIFF"
    I = tifffile.imread(ols) if ols.lower().endswith("tiff") else imageio.imread(ols)
    B = tifffile.imread(bse) if bse.lower().endswith("tiff") else imageio.imread(bse)
    I = I / 255.0
    print(np.min(I), np.max(I))
    # I = I[::fac, ::fac]
    # B = B[::fac, ::fac]
    L = tifffile.imread("./data/full_section/L-crop.TIFF")
    df = pd.read_csv('full_analysis_results/crystal_summary.csv')

    rev_ids = df["c-id"][df["c-type"] == 2]
    reg_ids = df["c-id"][df["c-type"] == 3]
    noz_ids = df["c-id"][df["c-type"] == 4]
    sub_ids = df["c-id"][df["c-type"] == 1]

    size_step = 200
    matrix_fac = 0.33

    for id in sub_ids:
        xmin, xmax, ymin, ymax = df[df["c-id"] == id].to_numpy()[0, -4:] * coords_fac + np.array(
            [CROP_TOP,
             CROP_BOTTOM,  # this parameter is not tested
             CROP_LEFT,
             CROP_RIGHT])  # this paramter is also not tested

        dx = xmax - xmin
        dy = ymax - ymin
        size = max(dx + 10, dy + 10)
        size = (size // size_step + 1) * size_step
        print(size)

        UP = xmin + dx // 2 - size // 2
        LE = ymin + dy // 2 - size // 2
        mask = I[UP:UP + size, LE:LE + size]
        mask = (mask + matrix_fac).clip(0, 1)
        OL = (B[UP:UP + size, LE:LE + size] * mask / 50000) * 255.0
        OL = OL.clip(0, 255).astype(np.ubyte)
        imageio.imsave(f'full_analysis_results/catalogue/sub/{id}.jpeg', OL)

    for id in rev_ids:
        xmin, xmax, ymin, ymax = df[df["c-id"] == id].to_numpy()[0, -4:] * coords_fac + np.array(
            [CROP_TOP,
             CROP_BOTTOM,  # this parameter is not tested
             CROP_LEFT,
             CROP_RIGHT])  # this paramter is also not tested

        dx = xmax - xmin
        dy = ymax - ymin
        size = max(dx + 10, dy + 10)
        size = (size // size_step + 1) * size_step
        print(size)

        UP = xmin + dx // 2 - size // 2
        LE = ymin + dy // 2 - size // 2
        mask = I[UP:UP + size, LE:LE + size]
        mask = (mask + matrix_fac).clip(0, 1)
        OL = (B[UP:UP + size, LE:LE + size] * mask / 50000) * 255.0
        OL = OL.clip(0, 255).astype(np.ubyte)
        imageio.imsave(f'full_analysis_results/catalogue/rev/{id}.jpeg', OL)

        # plt.imshow(OL, cmap='gray', vmin=0, vmax=255)
        # plt.show()

    for id in reg_ids:
        xmin, xmax, ymin, ymax = df[df["c-id"] == id].to_numpy()[0, -4:] * coords_fac + np.array(
            [CROP_TOP,
             CROP_BOTTOM,  # this parameter is not tested
             CROP_LEFT,
             CROP_RIGHT])  # this paramter is also not tested

        dx = xmax - xmin
        dy = ymax - ymin
        size = max(dx + 10, dy + 10)
        size = (size // size_step + 1) * size_step
        print(size)

        UP = xmin + dx // 2 - size // 2
        LE = ymin + dy // 2 - size // 2
        mask = I[UP:UP + size, LE:LE + size]
        mask = (mask + matrix_fac).clip(0, 1)
        OL = (B[UP:UP + size, LE:LE + size] * mask / 50000) * 255.0
        OL = OL.clip(0, 255).astype(np.ubyte)
        imageio.imsave(f'full_analysis_results/catalogue/reg/{id}.jpeg', OL)

    for id in noz_ids:
        xmin, xmax, ymin, ymax = df[df["c-id"] == id].to_numpy()[0, -4:] * coords_fac + np.array(
            [CROP_TOP,
             CROP_BOTTOM,  # this parameter is not tested
             CROP_LEFT,
             CROP_RIGHT])  # this paramter is also not tested

        dx = xmax - xmin
        dy = ymax - ymin
        size = max(dx + 10, dy + 10)
        size = (size // size_step + 1) * size_step
        print(size)

        UP = xmin + dx // 2 - size // 2
        LE = ymin + dy // 2 - size // 2
        mask = I[UP:UP + size, LE:LE + size]
        mask = (mask + matrix_fac).clip(0, 1)
        OL = (B[UP:UP + size, LE:LE + size] * mask / 50000) * 255.0
        OL = OL.clip(0, 255).astype(np.ubyte)
        imageio.imsave(f'full_analysis_results/catalogue/noz/{id}.jpeg', OL)


def make_cataloge_ts():
    fac = 1
    coords_fac = FAC

    ols = "./data/full_section/data_dlols.TIFF"  # path to binary map
    bse = "./data/full_section/data_CH0.TIFF"
    mgr = "./data/full_section/data_pred_MG_Nr.TIFF"
    I = tifffile.imread(ols) if ols.lower().endswith("tiff") else imageio.imread(ols)
    B = tifffile.imread(bse) if bse.lower().endswith("tiff") else imageio.imread(bse)
    N = tifffile.imread(mgr) if mgr.lower().endswith("tiff") else imageio.imread(mgr)

    I = I / 255.0
    print(np.min(I), np.max(I))
    # I = I[::fac, ::fac]
    # B = B[::fac, ::fac]
    L = tifffile.imread("./data/full_section/L-crop.TIFF")
    df = pd.read_csv('full_analysis_results/crystal_summary.csv')

    rev_ids = df["c-id"][df["c-type"] == 2]
    reg_ids = df["c-id"][df["c-type"] == 3]
    noz_ids = df["c-id"][df["c-type"] == 4]
    sub_ids = df["c-id"][df["c-type"] == 1]

    size_step = 200
    matrix_fac = 0.33

    for id in sub_ids:
        xmin, xmax, ymin, ymax = df[df["c-id"] == id].to_numpy()[0, -4:] * coords_fac + np.array(
            [CROP_TOP,
             CROP_BOTTOM,  # this parameter is not tested
             CROP_LEFT,
             CROP_RIGHT])  # this paramter is also not tested

        dx = xmax - xmin
        dy = ymax - ymin
        size = max(dx + 10, dy + 10)
        size = (size // size_step + 1) * size_step
        print(size)

        UP = xmin + dx // 2 - size // 2
        LE = ymin + dy // 2 - size // 2
        mask = I[UP:UP + size, LE:LE + size]
        mgn = N[UP:UP + size, LE:LE + size]
        OL = B[UP:UP + size, LE:LE + size]
        OL = np.dstack((OL, OL, OL)) / 50000 * 255.0
        OL[np.logical_and(mgn > 0.925, mask)] = [255, 255, 100]
        OL[np.logical_and(mgn < 0.535, mask)] = [100, 100, 255]
        mask = (mask + matrix_fac).clip(0, 1)[:, :, None]
        OL = OL * mask
        OL = OL.clip(0, 255).astype(np.ubyte)
        imageio.imsave(f'full_analysis_results/catalogue-ts/sub/{id}.jpeg', OL)

    for id in rev_ids:
        xmin, xmax, ymin, ymax = df[df["c-id"] == id].to_numpy()[0, -4:] * coords_fac + np.array(
            [CROP_TOP,
             CROP_BOTTOM,  # this parameter is not tested
             CROP_LEFT,
             CROP_RIGHT])  # this paramter is also not tested

        dx = xmax - xmin
        dy = ymax - ymin
        size = max(dx + 10, dy + 10)
        size = (size // size_step + 1) * size_step
        print(size)

        UP = xmin + dx // 2 - size // 2
        LE = ymin + dy // 2 - size // 2
        mask = I[UP:UP + size, LE:LE + size]
        mgn = N[UP:UP + size, LE:LE + size]
        OL = B[UP:UP + size, LE:LE + size]
        OL = np.dstack((OL, OL, OL)) / 50000 * 255.0
        OL[np.logical_and(mgn > 0.925, mask)] = [255, 255, 100]
        OL[np.logical_and(mgn < 0.535, mask)] = [100, 100, 255]
        mask = (mask + matrix_fac).clip(0, 1)[:, :, None]
        OL = OL * mask
        OL = OL.clip(0, 255).astype(np.ubyte)
        imageio.imsave(f'full_analysis_results/catalogue-ts/rev/{id}.jpeg', OL)

        # plt.imshow(OL, cmap='gray', vmin=0, vmax=255)
        # plt.show()

    for id in reg_ids:
        xmin, xmax, ymin, ymax = df[df["c-id"] == id].to_numpy()[0, -4:] * coords_fac + np.array(
            [CROP_TOP,
             CROP_BOTTOM,  # this parameter is not tested
             CROP_LEFT,
             CROP_RIGHT])  # this paramter is also not tested

        dx = xmax - xmin
        dy = ymax - ymin
        size = max(dx + 10, dy + 10)
        size = (size // size_step + 1) * size_step
        print(size)

        UP = xmin + dx // 2 - size // 2
        LE = ymin + dy // 2 - size // 2
        mask = I[UP:UP + size, LE:LE + size]
        mgn = N[UP:UP + size, LE:LE + size]
        OL = B[UP:UP + size, LE:LE + size]
        OL = np.dstack((OL, OL, OL)) / 50000 * 255.0
        OL[np.logical_and(mgn > 0.925, mask)] = [255, 255, 100]
        OL[np.logical_and(mgn < 0.535, mask)] = [100, 100, 255]
        mask = (mask + matrix_fac).clip(0, 1)[:, :, None]
        OL = OL * mask
        OL = OL.clip(0, 255).astype(np.ubyte)
        imageio.imsave(f'full_analysis_results/catalogue-ts/reg/{id}.jpeg', OL)

    for id in noz_ids:
        xmin, xmax, ymin, ymax = df[df["c-id"] == id].to_numpy()[0, -4:] * coords_fac + np.array(
            [CROP_TOP,
             CROP_BOTTOM,  # this parameter is not tested
             CROP_LEFT,
             CROP_RIGHT])  # this paramter is also not tested

        dx = xmax - xmin
        dy = ymax - ymin
        size = max(dx + 10, dy + 10)
        size = (size // size_step + 1) * size_step
        print(size)

        UP = xmin + dx // 2 - size // 2
        LE = ymin + dy // 2 - size // 2
        mask = I[UP:UP + size, LE:LE + size]
        mgn = N[UP:UP + size, LE:LE + size]
        OL = B[UP:UP + size, LE:LE + size]
        OL = np.dstack((OL, OL, OL)) / 50000 * 255.0
        OL[np.logical_and(mgn > 0.925, mask)] = [255, 255, 100]
        OL[np.logical_and(mgn < 0.535, mask)] = [100, 100, 255]
        mask = (mask + matrix_fac).clip(0, 1)[:, :, None]
        OL = OL * mask
        OL = OL.clip(0, 255).astype(np.ubyte)
        imageio.imsave(f'full_analysis_results/catalogue-ts/noz/{id}.jpeg', OL)


def make_cataloge_new(ols, bse, mgr, output):
    base_output_folder = os.path.join(output, 'catalogue_raw')
    coords_fac = FAC

    I = tifffile.imread(ols) if ols.lower().endswith("TIFF") else imageio.imread(ols)
    B = tifffile.imread(bse) if bse.lower().endswith("TIFF") else imageio.imread(bse)
    if os.path.exists(mgr):
        N = tifffile.imread(mgr) if mgr.lower().endswith("TIFF") else imageio.imread(mgr)
    else:
        N = None

    I = I / 255.0
    df = pd.read_csv(os.path.join(output, 'full_analysis_results/crystal_summary.csv'))

    rev_ids = df["c-id"][df["c-type"] == 2]
    reg_ids = df["c-id"][df["c-type"] == 3]
    noz_ids = df["c-id"][df["c-type"] == 4]
    sub_ids = df["c-id"][df["c-type"] == 1]

    size_step = 200
    matrix_fac = 0.33

    for id in sub_ids:
        xmin, xmax, ymin, ymax = df[df["c-id"] == id].to_numpy()[0, -4:] * coords_fac + np.array(
            [CROP_TOP,
             CROP_TOP,  # this parameter is not tested
             CROP_LEFT,
             CROP_LEFT])  # this paramter is also not tested

        dx = xmax - xmin
        dy = ymax - ymin

        size = max(dx + 10, dy + 10)
        size = (size // size_step + 1) * size_step
        print("Size: ", size)
        print("Mins: ", xmin, ymin)

        UP = max(xmin + dx // 2 - size // 2, 0)
        LE = max(ymin + dy // 2 - size // 2, 0)
        mask = I[UP:UP + size, LE:LE + size]
        mask = (mask + matrix_fac).clip(0, 1)
        OL = (B[UP:UP + size, LE:LE + size] * mask / 50000) * 255.0
        OL = OL.clip(0, 255).astype(np.ubyte)
        if min(OL.shape) < 5:
            continue
        output_folder = f'{base_output_folder}/sub'
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)

        imageio.imsave(f'{output_folder}/{id}_{UP}_{LE}.jpeg', OL)
        tifffile.imwrite(f'{output_folder}/{id}_{UP}_{LE}_mask.tiff', mask)
        tifffile.imwrite(f'{output_folder}/{id}_{UP}_{LE}_bse.tiff', B[UP:UP + size, LE:LE + size])
        if not N is None:
            tifffile.imwrite(f'{output_folder}/{id}_{UP}_{LE}_mgnr.tiff', N[UP:UP + size, LE:LE + size])

    for id in rev_ids:
        xmin, xmax, ymin, ymax = df[df["c-id"] == id].to_numpy()[0, -4:] * coords_fac + np.array(
            [CROP_TOP,
             CROP_BOTTOM,  # this parameter is not tested
             CROP_LEFT,
             CROP_RIGHT])  # this paramter is also not tested

        dx = xmax - xmin
        dy = ymax - ymin
        size = max(dx + 10, dy + 10)
        size = (size // size_step + 1) * size_step
        print(size)

        UP = max(xmin + dx // 2 - size // 2, 0)
        LE = max(ymin + dy // 2 - size // 2, 0)
        mask = I[UP:UP + size, LE:LE + size]
        mask = (mask + matrix_fac).clip(0, 1)
        OL = (B[UP:UP + size, LE:LE + size] * mask / 50000) * 255.0
        OL = OL.clip(0, 255).astype(np.ubyte)
        if min(OL.shape) < 5:
            continue
        output_folder = f'{base_output_folder}/rev'
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)
        imageio.imsave(f'{output_folder}/{id}_{UP}_{LE}.jpeg', OL)
        tifffile.imwrite(f'{output_folder}/{id}_{UP}_{LE}_mask.tiff', mask)
        tifffile.imwrite(f'{output_folder}/{id}_{UP}_{LE}_bse.tiff', B[UP:UP + size, LE:LE + size])
        if not N is None:
            tifffile.imwrite(f'{output_folder}/{id}_{UP}_{LE}_mgnr.tiff', N[UP:UP + size, LE:LE + size])

        # plt.imshow(OL, cmap='gray', vmin=0, vmax=255)
        # plt.show()

    for id in reg_ids:
        xmin, xmax, ymin, ymax = df[df["c-id"] == id].to_numpy()[0, -4:] * coords_fac + np.array(
            [CROP_TOP,
             CROP_BOTTOM,  # this parameter is not tested
             CROP_LEFT,
             CROP_RIGHT])  # this paramter is also not tested

        dx = xmax - xmin
        dy = ymax - ymin
        size = max(dx + 10, dy + 10)
        size = (size // size_step + 1) * size_step
        print(size)

        UP = max(xmin + dx // 2 - size // 2, 0)
        LE = max(ymin + dy // 2 - size // 2, 0)
        mask = I[UP:UP + size, LE:LE + size]
        mask = (mask + matrix_fac).clip(0, 1)
        OL = (B[UP:UP + size, LE:LE + size] * mask / 50000) * 255.0
        OL = OL.clip(0, 255).astype(np.ubyte)
        if min(OL.shape) < 5:
            continue
        output_folder = f'{base_output_folder}/reg'
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)
        imageio.imsave(f'{output_folder}/{id}_{UP}_{LE}.jpeg', OL)
        tifffile.imwrite(f'{output_folder}/{id}_{UP}_{LE}_mask.tiff', mask)
        tifffile.imwrite(f'{output_folder}/{id}_{UP}_{LE}_bse.tiff', B[UP:UP + size, LE:LE + size])
        if not N is None:
            tifffile.imwrite(f'{output_folder}/{id}_{UP}_{LE}_mgnr.tiff', N[UP:UP + size, LE:LE + size])

    for id in noz_ids:
        xmin, xmax, ymin, ymax = df[df["c-id"] == id].to_numpy()[0, -4:] * coords_fac + np.array(
            [CROP_TOP,
             CROP_BOTTOM,  # this parameter is not tested
             CROP_LEFT,
             CROP_RIGHT])  # this paramter is also not tested

        dx = xmax - xmin
        dy = ymax - ymin
        size = max(dx + 10, dy + 10)
        size = (size // size_step + 1) * size_step
        print("Siez: ", size)
        print("Mins: ", xmin, ymin)

        UP = max(xmin + dx // 2 - size // 2, 0)
        LE = max(ymin + dy // 2 - size // 2, 0)
        mask = I[UP:UP + size, LE:LE + size]
        mask = (mask + matrix_fac).clip(0, 1)
        OL = (B[UP:UP + size, LE:LE + size] * mask / 50000) * 255.0
        OL = OL.clip(0, 255).astype(np.ubyte)
        if min(OL.shape) < 5:
            continue
        output_folder = f'{base_output_folder}/noz'
        if not os.path.exists(output_folder):
            os.makedirs(output_folder)
        imageio.imsave(f'{output_folder}/{id}_{UP}_{LE}.jpeg', OL)

        tifffile.imwrite(f'{output_folder}/{id}_{UP}_{LE}_mask.tiff', mask)
        tifffile.imwrite(f'{output_folder}/{id}_{UP}_{LE}_bse.tiff', B[UP:UP + size, LE:LE + size])
        if not N is None:
            tifffile.imwrite(f'{output_folder}/{id}_{UP}_{LE}_mgnr.tiff', N[UP:UP + size, LE:LE + size])


def analyze(input_path):
    dlols  = os.path.join(input_path, 'oli_mask.TIFF')
    bse = os.path.join(input_path, 'stitched_CH0.TIFF')
    output_path = os.path.join(input_path, 'analyzed')
    if not os.path.exists(output_path):
        os.makedirs(output_path)
    else:
        return output_path


    analyze_full(ols=dlols,  # path to binary map
                 bse=bse,
                 output=output_path,
                 mgr="None")

    make_cataloge_new(ols=dlols,  # path to binary map
                      bse=bse,
                      output=output_path,
                      mgr="None")

    return output_path

#analyze('../media/173/tiff/stitched')

'''
if __name__ == "__main__":
    # analyze_sections()
    # kde_plot_all()
    # kde_plots()
    parser = argparse.ArgumentParser(description='Section analysis')
    parser.add_argument('input', nargs=1, help='Path to oli mask tiff file')
    args = parser.parse_args()
    dlols = args.input[0]
    bse = dlols.replace('_dlols', '_CH0')
    output = os.path.dirname(dlols) +"/.."
    analyze_full(ols=dlols,  # path to binary map
                 bse=bse,
                 output=output,
                 mgr="None")
    # kde_plot_full()


    make_cataloge_new(ols=dlols,  # path to binary map
                      bse=bse,
                      output=output,
                      mgr="None")
    # swarm_plot_full()
'''