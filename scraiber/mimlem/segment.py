import imageio
import matplotlib.pyplot as plt
import torch
from torchvision import models, transforms
from torchvision.models.segmentation.deeplabv3 import DeepLabHead
import os
import numpy as np


def createDeepLabv3(outputchannels):
    model = models.segmentation.deeplabv3_resnet101(
        pretrained=True, progress=True)
    model.classifier = DeepLabHead(2048, outputchannels)
    return model

def forward(model, data):
    if type(model) is models.segmentation.deeplabv3.DeepLabV3:
        return model(data)['out']
    return model(data)

class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample, maskresize=None, imageresize=None):
        image, mask = sample['image'], sample['mask']
        if not mask is None and len(mask.shape) == 2:
            mask = mask.reshape((1,) + mask.shape)
        if len(image.shape) == 2:
            image = image.reshape((1,) + image.shape)

        return {'image': torch.from_numpy(image.astype(np.float32)),
                'mask': None if mask is None else torch.from_numpy(mask),
                'image_id': sample['image_id']}


class Normalize(object):
    '''Normalize image'''

    def __init__(self, min, max):
        self.min = min
        self.max = max

    def __call__(self, sample):
        image, mask = sample['image'], sample['mask']
        image = torch.clamp(image, self.min, self.max)
        image = image.type(torch.FloatTensor)
        image -= self.min
        image /= self.max - self.min
        return {'image': image,
                'mask': mask,
                'image_id': sample['image_id']}

class DLSegmentor:  # not adopted for diferent models

    def __init__(self, modelpath, ch=1, min_norm=8891, max_norm=64949):
        self.model = createDeepLabv3(ch)
        self.device = torch.device('cpu')  # '"cuda:0" if torch.cuda.is_available() else "cpu")
        print("Device: {}".format(self.device))
        self.model.to(self.device)
        model_state = torch.load(modelpath, map_location=self.device)['model_state_dict']
        # print(model_state)
        self.model.load_state_dict(model_state)
        self.model.eval()
        self.transform = transforms.Compose(
            [ToTensor(),
             Normalize(min_norm, max_norm)])
        self.max_size = 1000
        self.stride = int(self.max_size / 1.2)

    def segment(self, bse, th=0.1, classes_to_seg=[1],):
        if self.max_size + self.stride < np.max(bse.shape):

            print('Image to large {shape:} splitting '.format(shape=bse.shape))
            vote_count = np.zeros(bse.shape, dtype=np.int)
            outputs = np.zeros(bse.shape, dtype=np.float)
            w_start = 0
            w_end = self.max_size
            while w_end < bse.shape[0]:
                h_start = 0
                h_end = self.max_size
                while h_end < bse.shape[1]:
                    if w_end + self.stride > bse.shape[0]:
                        w_end = bse.shape[0]
                    if h_end + self.stride > bse.shape[1]:
                        h_end = bse.shape[1]
                    part_img = bse[w_start: w_end, h_start: h_end]
                    _, part_outputs = self.segment(part_img, th, classes_to_seg)
                    outputs[w_start: w_end, h_start: h_end] += part_outputs
                    vote_count[w_start: w_end, h_start: h_end] += 1
                    h_start += self.stride
                    h_end += self.stride

                w_start += self.stride
                w_end += self.stride
            outputs /= vote_count
            return (outputs > th).astype(np.int), outputs  # todo: multiclass not implemented

        sample = {'image': np.array([bse, bse, bse]).astype(np.float32), 'mask': None, 'image_id': None}
        bse = self.transform(sample)['image']
        bse = bse[None, :, :, :]
        inputs = bse.to(self.device)
        outputs = forward(self.model, inputs)
        outputs = outputs.data.cpu().numpy()[0, 0, :, :]
        map = np.zeros(outputs.shape)
        mask = outputs >= th
        map[mask] = classes_to_seg[0]
        return map, outputs


def bse(in_path, out_path):
    segmentor = DLSegmentor(modelpath='/src/scraiber/mimlem/best_model_th03_augmented_pretrained.chkp')
    bse = imageio.imread(in_path)
    segmented, _ = segmentor.segment(bse)
    out_file = os.path.join(out_path, 'oli_mask.TIFF')
    imageio.imwrite(out_file, segmented)
    plt.imshow(segmented)
    plt.savefig(out_file.replace('.TIFF', '.png'))
    return segmented


def test_segment():
        segmentor = DLSegmentor(modelpath='best_model_th03_augmented_pretrained.chkp')
        bse = imageio.imread('../media/170/tiff/stitched/stiched_CH0.TIFF')
        segmented, prob_map = segmentor.segment(bse)
        print(segmented.shape)
        plt.imshow(segmented)
        plt.savefig('test_seg.png')
#import time
#start = time.time()
#test_segment()
#print("Seconds: ", time.time() - start)