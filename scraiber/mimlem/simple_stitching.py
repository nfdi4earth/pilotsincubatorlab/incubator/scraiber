import os
import shutil

import numpy as np
import imageio
from matplotlib import pyplot as plt
import tifffile
from meta import BCFMetadata
import glob
def stitch_auto(root, ofx=None, ofy=None):
    print("stitch")
    if not (ofx and ofy):
        file_meta_origin = f"{root}/Mapping_(1,1).yaml"
        bcfm_origin = BCFMetadata.from_yaml(file_meta_origin)
        meta_origin = bcfm_origin.original_metadata
        x_0 = meta_origin['Stage']['X']
        y_0 = meta_origin['Stage']['Y']
        dx = meta_origin['Microscope']['DX']
        dy = meta_origin['Microscope']['DY']
        width = meta_origin['DSP Configuration']['ImageWidth']
        height = meta_origin['DSP Configuration']['ImageHeight']
        print("dx, dy: ", dx, ' ', dy)
        file_meta_x = f"{root}/Mapping_(2,1).yaml"
        if os.path.exists(file_meta_x):
            print("Calc ofx")
            bcfm_x = BCFMetadata.from_yaml(file_meta_x)
            meta_origin = bcfm_x.original_metadata
            x_1 = meta_origin['Stage']['X']
            ofx = - int(((x_0 - x_1) // dx) - width)
        else:
            ofx = 0

        file_meta_y = f"{root}/Mapping_(1,2).yaml"
        if os.path.exists(file_meta_y):
            print("Calc ofy")
            bcfm_y = BCFMetadata.from_yaml(file_meta_y)
            meta_origin = bcfm_y.original_metadata
            y_1 = meta_origin['Stage']['Y']
            ofy = - int(((y_1 - y_0) // dy) - height)
        else:
            ofy = 0

    print("Estimated offset x, y: ", ofx, ofy)
    output_path = stitch(root, ofx, ofy)
    yamls = glob.glob(os.path.join(root, '*.yaml'))
    for yam in yamls:
        shutil.copy(src=yam, dst=output_path)
    return output_path

def stitch(root, ofx, ofy):
    target_folder = 'stitched'
    target_folder = os.path.join(root, target_folder)
    if not os.path.exists(target_folder):
        os.makedirs(target_folder)
    else:
        return target_folder

    h, w = imageio.imread(root + "/" + [f for f in os.listdir(f"{root}/") if f.endswith('.TIFF')][0]).shape[:2]

    ny, nx = np.max(np.array([((int(f.split("(")[1].split(",")[0]),
                                int(f.split(",")[1].split(")")[0])))
                              for f in os.listdir(f"{root}/") if "CH0" in f]), axis=0)

    for elem in ["CH0", "Fe", "Mg", "Na", "Si", "Al", "Ca"]:
        #dtype = np.uint16 if elem=="CH0" else np.float64
        stitched = np.zeros(((h - ofx) * nx + ofx, (w - ofy) * ny + ofy), dtype=np.float32)
        I = np.zeros((384,512))
        for x in range(1, nx + 1):
            for y in range(1, ny + 1):
                a_file = f"{root}/Mapping_({y},{x})_{elem}.TIFF"
                if os.path.exists(a_file):
                    I = tifffile.imread(a_file).astype(np.float32)
                else:
                    I = np.zeros(I.shape)
                stitched[(h - ofx) * (x - 1): (h - ofx) * (x - 1) + h, (w - ofy) * (y - 1):(w - ofy) * (y - 1) + w] = I
        if np.count_nonzero(stitched) < 100:
            print(f"No data found for {elem}")
            continue

        stitched = stitched.astype(np.uint16)
        format = 'TIFF'
        tifffile.imsave(f"{target_folder}/stitched_{elem}.{format}", stitched)
        plt.imshow(stitched, cmap='inferno')
        plt.colorbar()
        plt.tight_layout()
        plt.savefig(f"{target_folder}/stitched_{elem}.png")
        plt.clf()

    return target_folder



